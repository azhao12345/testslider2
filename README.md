# Future Slider
project miku slider firmware

### Features
* Distance based chain slide
* two finger slide mostly works


## Materials
* [FIGHTING BOARD PS3/PS4](http://www.brookaccessory.com/detail/58690501/)
* [CY8CKIT-043 PSoC® 4 M-Series Prototyping Kit](https://www.cypress.com/documentation/development-kitsboards/cy8ckit-043-psoc-4-m-series-prototyping-kit)
* [Slider PCB](https://github.com/azhao12345/testsliderpcb/releases)
* 1/8'' Polycarbonate cover

## Wiring 
The PSoC can run on 3.3v, but in this configuration, the brook board may be damaged if the PSoC is plugged into a PC.
![schematic](https://user-images.githubusercontent.com/6671151/53555069-0ca4bf00-3af6-11e9-940c-636a719b0e57.png)

## Compiling
To build with two finger touch support, build the project once, then change CAPSENSE_NUM_CENTROIDS to 2 in the generated configuration file.

## Configuration
| Option | Description |
| ------------- | ------------- |
| USE_WRAPPING | Wrap touch location (see below).  Disable this to send the touch location as-is. |
| USE_SMOOTHING | Smooth out touch location.  This will prevent swipes from registering for noise or small movement. |
| SMOOTHING_FACTOR | Amount of movement required to register.  Higher will require more movement to register, but will be less susceptible to noise. |

## Touch location mapping
The PS4 game will only partially fill some chain slides when the touchpad is swiped all the way across, while the AC version is able to fill all chain slides with a single swipe.  This USE_WRAPPING option will map the touch location 2:1 so that the transmitted distance of a swipe is doubled.  At certain points on the slider, the location is wrapped back.  The downside to this is that swiping past these wrapping locations will give a cool or good when wrong is deserved, and fill up a chain slide even in the wrong direction.  Disable this option to gain more accurate judgement, in exchange for chain slides not filling correctly.
![](https://user-images.githubusercontent.com/6671151/53691541-f59fe000-3d34-11e9-9631-c97a8caf5b86.png)

## Capsense Tuner
The capsense tuner will not work with two finger touch enabled.  The tuner can be used to verify all sensors are sensing properly and not shorted.

## Gallery
![](https://user-images.githubusercontent.com/6671151/53294311-30d06b00-3799-11e9-9d2b-647786c6367a.jpg)
![](https://user-images.githubusercontent.com/6671151/53294312-30d06b00-3799-11e9-8445-2382437770e1.jpg)
![](https://user-images.githubusercontent.com/6671151/53294313-30d06b00-3799-11e9-8d29-10dce0a3f063.jpg)
