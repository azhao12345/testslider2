/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdlib.h"

/* Boolean constants */
#define TRUE                            (1u)
#define FALSE                           (0u)

#define LED_ON                          (1u)
#define LED_OFF                         (0u)

/*Set the macro value to '1' to use tuner for debugging and tuning CapSense sensors
  Set the macro value to '0' to send the CapSense Linear slider and buttons data to EZ-BLE PRoC module via I2C*/
#define ENABLE_TUNER                    (1u)

/* miku slider configuration */
#define USE_WRAPPING                    (1u)
#define USE_SMOOTHING                   (1u)
#define SMOOTHING_FACTOR                (4u)

#if USE_WRAPPING
    #define wrap(x) ((((x) / 224) * 112) + (x) % 112)
#else
    #define wrap(x) ((x) / 2)
#endif

#define difference(x, y) ((uint32)(abs((int)(x) - (int)(y))))

/* Finite state machine states for device operating states */
typedef enum
{
    SENSOR_SCAN = 0x01u, /* Sensor is scanned in this state */
    WAIT_FOR_SCAN_COMPLETE = 0x02u, /* CPU is put to sleep in this state */
    PROCESS_DATA = 0x03u, /* Sensor data is processed */
    TRANSMIT = 0x4u
} DEVICE_STATE;

uint8 transmitBuffer[44];

int checkTransmit();

void processWidgetLocations();

#if USE_SMOOTHING
    uint32 anchor1 = CapSense_SLIDER_POS_NONE;
    uint32 anchor2 = CapSense_SLIDER_POS_NONE;
    uint8 anchor1hit = FALSE;
    uint8 anchor2hit = FALSE;

    uint8 hit(uint32* x) {
        if (*x == CapSense_SLIDER_POS_NONE) {
            return TRUE;
        }
        uint32 closest;
        uint8* closesthit;
        if (anchor1 == CapSense_SLIDER_POS_NONE) {
            return FALSE;
        } if (anchor2 == CapSense_SLIDER_POS_NONE) {
            closest = anchor1;
            closesthit = &anchor1hit;
        } else {
            uint32 midpoint = (anchor1 + anchor2) / 2;
            if (*x > midpoint) {
                closest = anchor2;
                closesthit = &anchor2hit;
            } else {
                closest = anchor1;
                closesthit = &anchor1hit;
            }
        }
        if (difference(closest, *x) <= SMOOTHING_FACTOR) {
            *closesthit = TRUE;
            *x = closest;
            return TRUE;
        }
        return FALSE;
    }
    /* assign miss helper */
    void assignMiss(uint32 x) {
        if (!anchor1hit) {
            anchor1 = x;
            anchor1hit = TRUE;
        } else {
            anchor2 = x;
            anchor2hit = TRUE;
        }
    }
    /* smoothing helper */
    void smooth(uint32* c1, uint32* c2) {
        anchor1hit = FALSE;
        anchor2hit = FALSE;
        uint8 c1hit = hit(c1);
        uint8 c2hit = hit(c2);
        
        if (!c1hit) {
            assignMiss(*c1);
        }
        if (!c2hit) {
            assignMiss(*c2);
        }
        
        if (!anchor1hit) {
            anchor1 = CapSense_SLIDER_POS_NONE;
        }
        if (!anchor2hit) {
            anchor2 = CapSense_SLIDER_POS_NONE;
        }
        
        if (!c1hit || !c2hit) {
            LED_Write(LED_ON);
        } else {
            LED_Write(LED_OFF);
        }
    }
#endif

int main(void)
{
        /* Variable to hold the current device state 
    *  State machine starts with sensor scan state after power-up
    */
    DEVICE_STATE currentState = SENSOR_SCAN; 
    
    /* Enable global interrupts for CapSense operation */
    CyGlobalIntEnable;
   
    /* Start EZI2C block */
    Tuner_i2c_Start();
    
    /* Set up communication data buffer to CapSense data structure to 
     * expose to I2C master at primary slave address request        
     */
    Tuner_i2c_EzI2CSetBuffer1(sizeof(CapSense_dsRam), sizeof(CapSense_dsRam),\
                         (uint8 *)&CapSense_dsRam);
    
    //brook_i2c_I2CSlaveInitReadBuf((uint8 *) transmitBuffer, 11u);
    brook_i2c_Start();

    /* Start CapSense block - Initializes CapSense data structure and 
     * performs first scan of all widgets/sensors to set up sensors
     * baselines 
     */
    CapSense_Start();

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        /* Place your application code here. */
         /* Switch between SENSOR_SCAN->WAIT_FOR_SCAN_COMPLETE->PROCESS_DATA states */
        switch(currentState)
        {
            case SENSOR_SCAN:
                /* Initiate new scan only if the CapSense block is idle */
                if(CapSense_NOT_BUSY == CapSense_IsBusy())
                {
                    #if ENABLE_TUNER
                        /* Update CapSense parameters set via CapSense tuner before the 
                           beginning of CapSense scan 
                        */
                        CapSense_RunTuner();
                    #endif
                    
                    /* Scan widget configured by CSDSetupWidget API */
                    CapSense_ScanAllWidgets();
                                        
                    /* Set next state to WAIT_FOR_SCAN_COMPLETE  */
                    currentState = WAIT_FOR_SCAN_COMPLETE;
                }
                break;

            case WAIT_FOR_SCAN_COMPLETE:

                /* Put the device to CPU Sleep until CapSense scanning is complete*/
                if(CapSense_NOT_BUSY != CapSense_IsBusy())
                {
                    CySysPmSleep();
                }
                /* If CapSense scanning is complete, process the CapSense data */
                else
                {
                    currentState = PROCESS_DATA;
                }
                break;
            case PROCESS_DATA:
                
                /* Process data on all the enabled widgets */
                CapSense_ProcessAllWidgets();
                
                processWidgetLocations();
                     
                /* Set the device state to transmit */
                currentState = TRANSMIT;
                break;
            case TRANSMIT:
                if(checkTransmit()) {
                    currentState = SENSOR_SCAN;
                }
                break;
            /*******************************************************************
             * Unknown power mode state. Unexpected situation.
             ******************************************************************/    
            default:
                break;
        } 
    }
}

int checkTransmit() {
    uint32 status = brook_i2c_I2CSlaveStatus();
    if (0u != (status & brook_i2c_I2C_SSTAT_RD_CMPLT)) {
        CyPins_SetPin(interrupt_0);
        return TRUE;
    }
    return FALSE;
}

void processWidgetLocations() {
    CapSense_RAM_WD_SLIDER_STRUCT *wdSlider;
    wdSlider = CapSense_dsFlash.wdgtArray[CapSense_SLIDER_WDGT_ID].ptr2WdgtRam;
    uint32 centroid1 = (uint32)wdSlider->position[0u];
    #if CapSense_NUM_CENTROIDS > 1u
        uint32 centroid2 = (uint32)wdSlider->position[1u];
    #else
        uint32 centroid2 = CapSense_SLIDER_POS_NONE;
    #endif
    
    if(centroid2 < centroid1) {
        uint32 temp = centroid2;
        centroid2 = centroid1;
        centroid1 = temp;
    }
    #if USE_SMOOTHING
        smooth(&centroid1, &centroid2);
    #endif
    
    int x;
    int y;
    if(centroid1 == CapSense_SLIDER_POS_NONE) {
        // no touch
        transmitBuffer[0] = 0x3c;
        transmitBuffer[1] = 0;
        transmitBuffer[2] = 0;
        brook_i2c_I2CSlaveInitReadBuf((uint8 *) transmitBuffer, 3u);
        //LED_Write(LED_OFF);
    } else if(centroid2 == CapSense_SLIDER_POS_NONE) {
        // one touch
        x = wrap(896 - centroid1);
        y = 5;

        transmitBuffer[0] = 0x3c;
        transmitBuffer[1] = 0x80;
        transmitBuffer[2] = 0;
        transmitBuffer[3] = ((x >> 4) & 0xF0) + ((y >> 8) & 0x0F); //x high 4, y high 4
        transmitBuffer[4] = x; // x low 8
        transmitBuffer[5] = y;
        transmitBuffer[6] = 30;
        brook_i2c_I2CSlaveInitReadBuf((uint8 *) transmitBuffer, 7u);

        //LED_Write(LED_ON);
    } else {
        // two touches
        x = wrap(896 - centroid1);
        y = 5;
        transmitBuffer[0] = 0x3c;
        transmitBuffer[1] = 0xC0;
        transmitBuffer[2] = 0;
        transmitBuffer[3] = ((x >> 4) & 0xF0) + ((y >> 8) & 0x0F); //x high 4, y high 4
        transmitBuffer[4] = x; // x low 8
        transmitBuffer[5] = y;
        transmitBuffer[6] = 30;
        y = 145;
        x = wrap(896 - centroid2);
        transmitBuffer[7] = ((x >> 4) & 0xF0) + ((y >> 8) & 0x0F); //x high 4, y high 4
        transmitBuffer[8] = x; // x low 8
        transmitBuffer[9] = y;
        transmitBuffer[10] = 30;
        brook_i2c_I2CSlaveInitReadBuf((uint8 *) transmitBuffer, 11u);
        //LED_Write(LED_OFF);
    }
    brook_i2c_I2CSlaveClearReadStatus();
    CyPins_ClearPin(interrupt_0);
}

/* [] END OF FILE */
